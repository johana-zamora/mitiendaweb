FROM node:14.15.0
WORKDIR /usr/src/app
COPY . .
RUN npm i -g pm2
RUN cd api && npm install
RUN npm install @angular/cli && npm install
RUN ./node_modules/.bin/ngcc --properties es2015
#&& npm run build
EXPOSE 3000
#CMD [ "node", "api/index.js" ]
ENTRYPOINT ["tail", "-f", "/dev/null"]
