Stop process node 
----

ps aux | grep node
kill pid


PM2 para modo Produccion
-----
Para iniciar el servidor
pm2 start api/index.js

Para detener
pm2 kill
