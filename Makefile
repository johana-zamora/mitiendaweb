#!make
MAKEFLAGS += --silent
runserver:
		docker-compose up -d --build
		docker exec -it -e VAR=1 ecommerce_app bash

reset-db:
		docker exec ecommerce_db mysql -uroot -proot -e "DROP DATABASE IF EXISTS ecommerce;"
		docker exec ecommerce_db mysql -uroot -proot -e "CREATE DATABASE ecommerce;"
		docker exec ecommerce_db bash -c "mysql -uroot -proot ecommerce < /tmp/var/esquema.sql"

build-prod:
		docker-compose up -d --build

run-prod:
		docker exec ecommerce_app bash -c "pm2 start api/index.js"

stop-prod:
		docker exec ecommerce_app bash -c "pm2 kill"


.PHONY: runserver
